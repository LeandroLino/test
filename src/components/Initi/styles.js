import styled from 'styled-components'

const Container = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-flow: row wrap;
    flex-direction: column;

    width: 100%;
    height: 100vh;
`;

Container.Name = styled.div`
    display: flex;
    text-align: center;

    color: white;
    font-size: 3.5em;
    font-family: 'Roboto Mono', monospace;
    font-weight: bold;
    >span{
        color: rgb(252, 163, 17);
        padding: 5px;
    }
`;

Container.Especialization = styled.div`
    display: flex;
    text-align: center;

    color: white;
    font-size: 3em;
    font-family: 'Roboto Mono', monospace;
    font-weight: 100;
    font-style: italic;
`;
Container.Hard = styled.div`
    background-color: rgba(255,255,255, 0.1);
    border-radius: 14px;

    width: 80%;

    display: flex;
    justify-content: center;
    align-items: center;
    flex-flow: row wrap;

    >svg{
        padding: 10px;

        color: white;
        width: 50px;
        height: 50px;

        @media(max-width: 960px) {
            padding: 5px;

            width: 30px;
            height: 30px;
        }
    }
    >span:last-child{
        background-image: linear-gradient(90deg, rgb(252, 163, 17), transparent);
        width: 10%;
        height: 2px;
        background-size: 100% 2px;
        background-repeat: no-repeat;
    }
    >span:first-child{
        background-image: linear-gradient(90deg, transparent, rgb(252, 163, 17));
        width: 10%;
        height: 2px;
        background-size: 100% 2px;
        background-repeat: no-repeat;
    }
    >svg:nth-child(2){
        color: yellow;
    }
    >svg:nth-child(3){
        color: #ff6700;
    }
    >svg:nth-child(4){
        color: #0077b6;
    }
    >svg:nth-child(5){
        color: #00b4d8;
    }
    >svg:nth-child(6){
        color: #9d4edd;
    }
    >svg:nth-child(7){
        color: #fb8500;
    }
    >svg:nth-child(8){
        color: #fff;
    }
    >svg:nth-child(9){
        color: #ffd60a;
    }
    >svg:nth-child(11){
        color: #023e8a;
    }
    >svg:nth-child(12){
        color: #1b4332;
    }
    >svg:nth-child(13){
        color: #48cae4;
    }
    `;
export default Container