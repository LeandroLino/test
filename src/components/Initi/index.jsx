import Container from './styles'
import { FaReact } from 'react-icons/fa';
import { SiJavascript, SiHtml5, SiGitlab, SiPython, SiCsswizardry,
SiRedux, SiNodeDotJs, SiFlask, SiPostgresql } from 'react-icons/si';
import { DiScrum } from 'react-icons/di';
import { IoLogoVercel } from 'react-icons/io5';

const Init = () => {
    return (
        <Container>
            <Container.Name>
                <span>&lt;</span>Leandro Lino<span>/&gt;</span>
            </Container.Name>
            <Container.Especialization>
                Full Stack Developer
            </Container.Especialization>
            <Container.Hard>
                <span></span>
                <SiJavascript/>
                <SiHtml5/>
                <SiCsswizardry/>
                <FaReact/>
                <SiRedux/>
                <SiGitlab/>
                <IoLogoVercel/>
                <SiPython/>
                <SiFlask/>
                <SiPostgresql/>
                <SiNodeDotJs/>
                <DiScrum/>
                <span></span>
            </Container.Hard>
        </Container>
    )
}
export default Init