import Container from './style'
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';
import kenzieHub from '../../assets/kenzieHub.png'
import matoFy from '../../assets/matoFy.png'
const AboutMe = () => {
    return (
        <Container>
            <Carousel>
                <Container.Carousel>
                    <img src={kenzieHub} />
                    <Container.Button>Legend 1</Container.Button>
                </Container.Carousel>
                <Container.Carousel>
                    <img src={matoFy}/>
                    <Container.Button>Legend 2</Container.Button>
                </Container.Carousel>
                <Container.Carousel>
                    <img src="https://miro.medium.com/max/498/1*5gJzummAqpBDGATo0fjU6Q.jpeg" />
                    <Container.Button>Legend 3</Container.Button>
                </Container.Carousel>
            </Carousel>
        </Container>
    )
}

export default AboutMe



            
