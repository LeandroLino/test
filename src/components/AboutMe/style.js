import styled from 'styled-components'

const Container = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;

    width: 100%;
    height: 100vh;

    background-color: rgba(17,18,22, 0.5);

    >:nth-child(1){
        width: 50%;
        height: 50%;
        >div ul {
            padding-top: 25px;
        }
        >div div ul li div{
            height: 40vh;
        }
    }
`;
Container.Button = styled.div`
    width: 15%;
    height: 20px !important;

    background-color: red;
`;

Container.Carousel = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
`;


export default Container