import React from 'react';
import './App.css';
import AboutMe from './components/AboutMe';
import Init from './components/Initi';

const App = () => {
  return (
    <div>
      <Init/>
      <AboutMe/>
    </div>
  );
}

export default App;
